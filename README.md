# ufpr-boticario-2020-02

Informações e instruções sobre o Hackathon realizado na UFPR, utilizando como case uma base de dados do Boticário.

## Introdução

Para este hackathon, o Boticário nos forneceu uma base de vendas semanais, quebrada por cidade, produto e canal de venda. A motivação da empresa é conseguir prever o valor vendido (i.e. receita) em algum momento futuro.

## Dados
Forneceremos acesso a 3 arquivos:

 - train.csv
 - test.csv
 - ibge-data.xlsx

O arquivo *train.csv* possui dados de venda desde o começo de 2017 até a metade de 2019, enquanto o arquivo *test.csv* diz respeito ao segundo semestre de 2019 (porém, sem a informação de venda). Forneceremos também uma base simplificada do IBGE, que possui informação sobre a população de cada cidade. Dúvidas sobre os dados podem ser tiradas com os monitores do evento.

O campo que estamos interessados em prever é o campo *Valor* e avaliaremos o resultado dos modelos utilizando o MAE (i.e. Mean Absolute Error).

## Desenvolvimento

Para este hackathon, sugerimos fortemente que todos utilizem o [Google Colab](https://colab.research.google.com/).

### Acesso aos Dados

Para acessar a base de dados dentro do ambiente do Google Colab precisamos baixar os dados para o sandbox. Para isso, iremos nos utilizar do SDK do Google Cloud, o que exige que algum usuário que possui conta no google confirme o uso deste SDK, obtenha o código de verificação fornecido pela Google e cole esse código no campo que irá aparecer no Colab.

Para realizar o procedimento acima, execute o código abaixo dentro do Colab:

```python
!pip install -U -q PyDrive
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from google.colab import auth
from oauth2client.client import GoogleCredentials

# authenticate and create the PyDrive client
auth.authenticate_user()
gauth = GoogleAuth()
gauth.credentials = GoogleCredentials.get_application_default()
drive = GoogleDrive(gauth)

# download data to sandbox
IBGE_DATA_ID = '12S15y1eBuCh9OK06yO8NtIZ_CYBFtPJh'
downloaded = drive.CreateFile({'id': IBGE_DATA_ID})
downloaded.GetContentFile('ibge-data.xlsx')

TRAIN_DATA_ID = '1NdGTKyEs9TKam3Qp4hEqqmkH-x722LpQ'
downloaded = drive.CreateFile({'id': TRAIN_DATA_ID})
downloaded.GetContentFile('train.csv')

TEST_DATA_ID = '1C6bwzS3ZfdVUU_feH7RejZF6QxNLJxiE'
downloaded = drive.CreateFile({'id': TEST_DATA_ID})
downloaded.GetContentFile('test.csv')
```

Esse código baixa os 3 arquivos mencionados anteriomente para o ambiente do Colab, permitindo que você desenvolva toda a parte de modelagem dentro deste ambiente.


## Submissão

O resultado do modelo deve ser salvo num arquivo que segue, NECESSARIAMENTE, o seguinte formato:

```csv
Ano,Semana,Canal_Comercial,Código IBGE,Produto,Valor
2019,27,AZUL,2700300,Maca,53.52
2019,27,AZUL,2700409,Maca,979.88
2019,27,AZUL,2700607,Maca,53.08
2019,27,AZUL,2701001,Maca,20.72
2019,27,AZUL,2702207,Maca,174.85
...
```
e que deve ser nomeado *output-<token_equipe>.csv*, onde *<token_equipe>* representa um código que será entregue a cada uma das equipes depois que o hackathon já tiver iniciado.

Para submeter o arquivo, utilize a seguinte função:

```
!pip install -U -q boto3
import boto3

def upload_submission(filename):
    aws_access_key = 'AKIAYSXYXW3OSPD2RRSL'
    aws_secret_key = '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'

    s3r = boto3.resource('s3', aws_access_key_id=aws_access_key,
                               aws_secret_access_key=aws_secret_key)

    bucket = s3r.Bucket('data-boticario')

    with open(filename, 'rb') as f:
        bucket.upload_fileobj(f, 'submissions/'+filename)
```

Apenas a última submissão de cada equipe será considerada, e o ranking das equipes só será liberado após o prazo final de submissão.
